# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 20:28:44 2020
Clase usada para interactuar y manipular la pagina de resultados despues de
realizar una busqueda en la pagina principal.
@author: Hugo
"""

from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


class ResultsPage:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 10)
        self.xpath_title = "//h3[@class='LC20lb DKV0Md']"
        self.xpath_correction = "//p[@class='p64x9c card-section']"
        self.xpath_correction_link = "//a[@class='gL9Hy']"
        self.xpath_description = "//span[@class='st']"
        self.xpath_input_search = "//input[@class='gLFyf gsfi']"
        self.xpath_no_results_txt = ("//p[contains(text(),'No se han"
        "encontrado resultados para tu búsqueda (')]")
        self.descriptions = []
        self.titles = []

    # metodo para validar si existen resultados de la busqueda
    def validate_if_results(self):
        try:
            self.driver.find_element_by_xpath(self.xpath_no_results_txt)
            return False
        except NoSuchElementException:
            return True

    # imprime los resultados
    def print_results(self):
        print("Resultados:")

        self.wait.until(
            ec.presence_of_all_elements_located((By.XPATH, self.xpath_title)))

        for x in self.titles:

            if x.text != "":
                print("-----------------------------------")
                print(x.text + "\n")
                print(self.descriptions[self.titles.index(x)].text)

        return True

    # valida si google ofrece una correccion, si la hay hace click sobre ella
    def validate_correction_message(self):

        self.wait.until(
            ec.presence_of_all_elements_located((By.XPATH, self.xpath_title)))
        print("Buscando Correccion...")
        try:
            self.driver.find_element_by_xpath(self.xpath_correction)
            aux_flag = self.click_on_correction()
            print("Correccion ofrecida")
            return aux_flag
        except NoSuchElementException:
            print("Google no ofrecio una correccion")
            return True

    # metodo auxiliar para dar click sobre la correccion
    def click_on_correction(self):

        bttn = self.driver.find_element_by_xpath(self.xpath_correction_link)
        bttn.click()
        try:
            self.wait.until(
                ec.presence_of_all_elements_located((By.XPATH,
                                                     self.xpath_title)))
            return True
        except NoSuchElementException:
            return False

    # valida que existan las descripciones
    def validate_description(self):

        self.descriptions = self.driver.find_elements_by_xpath(
            self.xpath_description)
        self.titles = self.driver.find_elements_by_xpath(self.xpath_title)

        descriptions_size = len(self.descriptions)
        titles_size_size = len(self.titles)

        if descriptions_size == titles_size_size:
            return True
        else:
            return False

    # valida que el titulo concuerde con la busqueda realizada
    def validate_title(self):

        search = self.driver.find_element_by_xpath(
            self.xpath_input_search).get_attribute('value')

        for x in self.titles:
            if search in x.text:
                return True
        return False
