"""
Created on Fri Apr  3 18:38:36 2020
Clase principal usada para realizar el test.
@author: Hugo
"""
from poms.google_home_page import GoogleHomePage as hp
from setup.setup import Setup
from poms.results_page import ResultsPage as rp
import unittest


class GoogleSearchTest(unittest.TestCase):

    def test_main(self):

        print("Iniciando prueba...")
        try:
            url = "https://www.google.com/"
            chrome = Setup("ch")
            chrome = chrome.get_driver()
            chrome.get(url)

            home_page = hp(chrome)
            self.assertIsNotNone(home_page.search_word(),
                                 "No se encontro la barra de busqueda")
            results_page = rp(chrome)
            self.assertTrue(results_page.validate_if_results(),
                            "Busqueda sin resultados")
            self.assertTrue(results_page.validate_correction_message(),
                            "Error al interactuar con la correccion")
            self.assertTrue(results_page.validate_description(),
                            "Error al obtener las descripciones")
            self.assertTrue(results_page.validate_title(),
                            "No hubo coincidencias con la busqueda")
            self.assertTrue(results_page.print_results(),
                            "Error al imprimir los resultados")

            chrome.quit()
        except Exception as e:
            print(e)


if __name__ == "__main__":
    unittest.main()
